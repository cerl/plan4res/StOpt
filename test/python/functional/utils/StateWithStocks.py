# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)

# Permits to define a state containing
#   - a stock positipon
#   - a realisation of the non controled stochastic state


# State class dealing with regime, stock and uncertainty
class StateWithStocks :
    
    # constructor
    def __init__(self, p_regime, p_ptStock, p_stochasticRealisation) :
        
        self.m_ptStock = p_ptStock
        self.m_stochasticRealisation = p_stochasticRealisation
        self.m_regime = p_regime
        
    # accessor

    def getPtStock(self) :
        
        return self.m_ptStock
    
    def getStochasticRealization(self) :
        
        return self.m_stochasticRealisation
    
    def getRegime(self) :
        
        return self.m_regime
    
    def setPtStock(self, p_ptStock) :
        
        self.m_ptStock = p_ptStock
        
    def setStochasticRealization(self, p_stochasticRealisation) :
        
        self.m_stochasticRealisation = p_stochasticRealisation
        
    def setRegime(self, p_regime) :
        
        self.m_regime = p_regime
