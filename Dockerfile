ARG IMAGE_REGISTRY=docker.io
ARG IMAGE_NAME=jupyter/minimal-notebook
ARG IMAGE_TAG=latest

FROM ${IMAGE_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG}

RUN pip install -U pip && \
    pip install \
        numpy \
        scipy \
        pybind11-cmake \
        pybind11

USER root

RUN apt-get update &&  \
    apt-get install -y \
        gcc \
        cmake \
        libeigen3-dev \
        zlibc \
        zlib1g-dev \
        libbz2-dev \
        libboost-log-dev \
        libboost-thread-dev \
        libboost-test-dev \
        libboost-timer-dev \
        libboost-serialization-dev \
        libboost-random-dev \
        libboost-chrono-dev \
        python3.7-dev \
        python3-pybind11 \
        pybind11-dev && \
    rm -rf /var/lib/apt/lists/*

RUN git clone \
        --depth=1 \
        -b master \
            https://gitlab.com/stochastic-control/StOpt.git \
            /tmp/StOpt && \
    mkdir -p /opt/StOpt && \
    cd /opt/StOpt && \
    cmake /tmp/StOpt \
        -DPYTHON_LIBRARY=/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu/libpython3.7.so && \
    make && \
    rm -rf /tmp/StOpt

RUN ln -s /opt/StOpt/lib/* /opt/conda/lib/python3.7/

USER jovyan
