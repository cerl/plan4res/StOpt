Instruction for R Stopt Regression package installation  for Linux and Mac users
################################################################################
Prerequisite :
Install R package under R :
"""
install.packages("Rcpp")
install.packages("RcppEigen")
"""
or in a given place
"""
install.packages("Rcpp", lib="install_dir")
library(Rcpp ,lib.loc="install_dir")
install.packages("RcppEigen", lib="install_dir")
library(RcppEigen, lib="install_dir")
"""
StOpt library should be compiled :

# compilation
#############
in R/StOptRegression/src
change Makevars according to your StOpt installation :
"""
PKG_LIBS = -LSTOPTLIB -lStOpt
PKG_CPPFLAGS= -ISTOPTINC  -std=c++14
""
where STOPTLIB is where STOPTLIB refers to the StOpt library directory
      STOPTINC is the top of StOpt directory headers installation
Then
in R directory :
R CMD INSTALL StOptRegression

or

R CMD INSTALL -l install_dir  StOptRegression


# USE
######
Syntax same as python and c++ interface.
See python examples.
Constructor :
Under R :
"""
require( Rcpp)
require( RcppEigen )
require(StOptRegression)

// particles
particles = matrix(1.:10.,1,10)
// number of mesh per direction
mesh = c(2)
// regressor
reg= new(LocalLinearRegression,0,particles,mesh)
// to regress
x = c(-0.438185, -0.766791, 0.695282, 5., 2., 1.,7., 9., 8.9, 3.)
reg$getAllSimulations(x)
"""
